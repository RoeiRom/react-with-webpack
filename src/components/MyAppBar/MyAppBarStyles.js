import { makeStyles } from '@material-ui/core';

const useStyle = makeStyles({
    appBar: {
        height: '5vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default useStyle;