import React from 'react';
import { AppBar, Typography } from '@material-ui/core';

import useStyle from './MyAppBarStyles';

const MyAppBar = () => {
    
    const classes = useStyle();

    return (
        <AppBar className={classes.appBar}>
            <Typography>My Webpack React app!</Typography>
        </AppBar>
    )
};

export default MyAppBar;