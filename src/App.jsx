import React from 'react';

import MyAppBar from './components/MyAppBar/MyAppBar';

const App = () => {
    return (
        <MyAppBar />
    )
}

export default App;